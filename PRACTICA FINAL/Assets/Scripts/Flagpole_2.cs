using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flagpole_2 : MonoBehaviour
{
    public Animator animator;
    public Transform Player;

    private Vector2 contact;
    private bool marked;
   // private bool first;
   //// private bool second;
   // private bool third;
   // private bool fourth;
   // private bool fifth;

    // Start is called before the first frame update
    void Start()
    {

        marked = false;
       // first = false;
       // second = false;
       // third = false;
       // fourth = false;
        //fifth = false;

    }

    // Update is called once per frame
    void Update()
    {


        if (marked)
        {





            if (Player.transform.position.y >3.2)
            {
                marked = false;
                this.GetComponent<BoxCollider2D>().isTrigger = true;

                animator.SetBool("FromTop", true);
                TotalScoreManager.instance.UpdateValue(5000);
                StartCoroutine(End());
            }
            else if (Player.transform.position.y > 2.2)
            {
                marked = false;
                this.GetComponent<BoxCollider2D>().isTrigger = true;

                animator.SetBool("second", true);
                TotalScoreManager.instance.UpdateValue(2000);

                StartCoroutine(End());
            }
            else if (Player.transform.position.y > 0.2)
            {
                marked = false;
                this.GetComponent<BoxCollider2D>().isTrigger = true;
                animator.SetBool("third", true);
                TotalScoreManager.instance.UpdateValue(800);


                StartCoroutine(End());
            }
            else if (Player.transform.position.y > -1.8)
            {
                marked = false;
                this.GetComponent<BoxCollider2D>().isTrigger = true;
                animator.SetBool("fourth", true);
                TotalScoreManager.instance.UpdateValue(400);

                StartCoroutine(End());

            }
            else
            {
                marked = false;
                this.GetComponent<BoxCollider2D>().isTrigger = true;

                animator.SetBool("fifth", true);
                TotalScoreManager.instance.UpdateValue(100);

                StartCoroutine(End());
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.CompareTag("jugador")){
            marked = true;
        }
        

    }

    IEnumerator End()
    {


        yield return new WaitForSeconds(1f);
        animator.SetBool("end", true);

    }


}
