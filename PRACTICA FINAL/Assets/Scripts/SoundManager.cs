using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SoundManager : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] Slider volumeSlider;
    [SerializeField] Slider SFXvolumeSlider;

    void Start()
    {
        if(!PlayerPrefs.HasKey("musicVolume"))
        {
            PlayerPrefs.SetFloat("musicVolume", 1);
            Load();
        }
        else
        {
            Load();
        }
        /* not making sfx since they drive me nuts
         * 
        if (!PlayerPrefs.HasKey("SFXVolume"))
        {
            PlayerPrefs.SetFloat("SFXVolume", 1);
            Load();
        }
        else
        {
            Load();
        }
        */

    }

    // This for music
    public void ChangeVolume()
    {
        AudioListener.volume = volumeSlider.value;
    }
    private void Load()
    {
        volumeSlider.value = PlayerPrefs.GetFloat("musicVolume");
    }
    private void Save()
    {
        PlayerPrefs.SetFloat("musicVolume", volumeSlider.value);
    }
    /*
    //This for SFX
   public void SFXChangeVolume()
    {
        AudioListener.volume = SFXvolumeSlider.value;
    }
    private void SFXLoad()
    {
        SFXvolumeSlider.value = PlayerPrefs.GetFloat("SFXmusicVolume");
    }
    private void SFXSave()
    {
    PlayerPrefs.SetFloat("SFXmusicVolume", SFXvolumeSlider.value);
    }

    */


}
