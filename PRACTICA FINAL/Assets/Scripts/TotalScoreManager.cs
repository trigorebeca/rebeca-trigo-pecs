using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class TotalScoreManager : MonoBehaviour
{
    public static TotalScoreManager instance;
    public TextMeshProUGUI text;
    private int addScore;
    private int totalScore;
    private string finalScore;

    public int currentScore;



    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        
        currentScore = PlayerPrefs.GetInt("finalScore");
        if (currentScore > 0)
        {
            UpdateValue(currentScore);
        }

    }



    

    public void UpdateValue(int addScore)
    {
        totalScore += addScore;

        text.text = "MARIO " + "\n" + totalScore.ToString();
        PlayerPrefs.SetInt("finalScore", totalScore);
    }





  

 
}
