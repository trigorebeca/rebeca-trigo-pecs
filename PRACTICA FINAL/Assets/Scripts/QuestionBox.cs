using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionBox : MonoBehaviour
{ 

    public float bounceHeight ;
    public float bounceSpeed;

    private Vector2 originalPosition;

    private bool opened;

    public Animator animator;
    public GameObject coin;
    public GameObject mushroom;
    public GameObject flower;
    public GameObject star;

    public Transform objectSpawn;
    //r from reward to select in unity the reward for each block manually
    public bool coinR;
    public bool mushroomR;
    public bool flowerR;
    public bool starR;


    // Start is called before the first frame update
    void Start()
    {
        originalPosition = transform.localPosition;
        opened = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("jugador") && collision.contacts[0].normal.y > 0.5f && !opened)
        {
            if(coin != null && coinR)
            {
                var coinValue = 1;
                Instantiate(coin, objectSpawn.position, objectSpawn.rotation);
                ScoreManager.instance.UpdateValue(coinValue);
                TotalScoreManager.instance.UpdateValue(100);

                coinR = false;

            }
            else if (mushroom != null && mushroomR)
            {
                Instantiate(mushroom, objectSpawn.position, objectSpawn.rotation);
                mushroomR = false;

            }
            if (star != null && starR)

            {
                Instantiate(star, objectSpawn.position, objectSpawn.rotation);
                starR = false;

            }
            if (flower != null && flowerR)
            {
                Instantiate(flower, objectSpawn.position, objectSpawn.rotation);
                flowerR = false;

            }

            Invoke("Bounce", (float)(0.1));
            Invoke("Back", (float)(0.2));
            Invoke("Opened", (float)(0.3));

        }
    }

    void Bounce()
    {
        
        transform.localPosition = new Vector2(originalPosition.x, transform.localPosition.y + bounceHeight);

    }

    void Back()
    {
        transform.localPosition = originalPosition;
    }
    void Opened()
    {
        opened = true;
        animator.SetBool("used", opened);

    }




}

