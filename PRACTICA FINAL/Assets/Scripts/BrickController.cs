using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickController : MonoBehaviour
{

    public float bounceHeight;
    public float bounceSpeed;

    private Vector2 originalPosition;

    private bool canBounce = true;

    private ParticleSystem particle;
    private SpriteRenderer sr;
    private BoxCollider2D bc;
    public GameObject coin;
    public GameObject mushroom;
    public GameObject flower;
    public GameObject star;
    public Transform objectSpawn;
    //r from reward to select in unity the reward for each block manually
    public bool coinR;
    public bool mushroomR;
    public bool flowerR;
    public bool starR;

    // Start is called before the first frame update
    void Start()
    {
        originalPosition = transform.localPosition;
        particle = GetComponentInChildren<ParticleSystem>();
        sr = GetComponent<SpriteRenderer>();
        bc = GetComponent<BoxCollider2D>();
        canBounce = true;

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("jugador") && collision.contacts[0].normal.y > 0.5f)
        {
            if (PlayerController.big)
            {

                StartCoroutine(Break());

            }
            else
            {
                if (canBounce)
                {
                    canBounce = false;
                    StartCoroutine(Bounce());
                }
            }
            if (coin != null && coinR)
            {
                var coinValue = 1;
                Instantiate(coin, objectSpawn.position, objectSpawn.rotation);
                ScoreManager.instance.UpdateValue(coinValue);
                TotalScoreManager.instance.UpdateValue(100);

                coinR = false;

            }
            else if (mushroom != null && mushroomR)
            {
                Instantiate(mushroom, objectSpawn.position, objectSpawn.rotation);
                mushroomR = false;
            }
            if (star != null && starR)

            {
                Instantiate(star, objectSpawn.position, objectSpawn.rotation);
                starR = false;

            }
            if (flower != null && flowerR)
            {
                Instantiate(flower, objectSpawn.position, objectSpawn.rotation);
                flowerR = false;

            }

        }
    }

    private IEnumerator Bounce()
    {
        while(true)
        {
            transform.localPosition = new Vector2(originalPosition.x, transform.localPosition.y + bounceSpeed * Time.deltaTime);
            if (transform.localPosition.y >= originalPosition.y + bounceHeight)
            {
                break;
            }
        yield return null;

        }

        while (true)
        {
            transform.localPosition = new Vector2(originalPosition.x, transform.localPosition.y + bounceSpeed * Time.deltaTime);
            if (transform.localPosition.y >= originalPosition.y)
            {
                transform.localPosition = originalPosition;
                break;

            }
            yield return null;

        }

        canBounce = true;

    }
    private IEnumerator Break()
    {
        particle.Play();
        sr.enabled = false;
        bc.enabled = false;


        yield return new WaitForSeconds(particle.main.startLifetime.constantMax);
        Destroy(gameObject);
    }
}
