﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicControlScript : MonoBehaviour
{
    public static MusicControlScript music;


    private void Awake() 
    {
        DontDestroyOnLoad(this.gameObject); // Don't destroy this gameObject when loading different scenes
        if (music == null)
        {
            DontDestroyOnLoad(gameObject);
            music = this;
        }
        else if (music != this)
        {
            Destroy(gameObject);
        }


    }



}
