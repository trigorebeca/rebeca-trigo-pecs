using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MushroomController : MonoBehaviour
{
    public float Speed = 1f;
    public bool moveLeft = false;
    public bool generated;
    private GameObject jugador;

    private Animator animator;
    public BoxCollider2D bc;
    private Rigidbody2D rb;




    // Start is called before the first frame update
    void Start()
    {
        jugador = GameObject.FindGameObjectWithTag("jugador");
        animator = GetComponentInChildren<Animator>();
        rb = GetComponent<Rigidbody2D>();
        bc.enabled = false;
        generated = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (generated)
        {
            animator.SetBool("Generated", generated);
            Speed = 0;
            StartCoroutine(Move());
            rb.bodyType = RigidbodyType2D.Kinematic;

        }

        if (moveLeft)
        {
            transform.Translate(-2 * Time.deltaTime * Speed, 0, 0);
        }
        else
        {
            transform.Translate(2 * Time.deltaTime * Speed, 0, 0);
        }
    }
    private void OnTriggerEnter2D (Collider2D collision)
    {
        if (collision.gameObject.CompareTag("tube"))
        {
            if (moveLeft)
            {
                moveLeft = false;
            }
            else
            {
                moveLeft = true;
            }
        }
        if (collision.gameObject.tag == "jugador")
        {
            Destroy(gameObject);
            PlayerController.big = true;
            TotalScoreManager.instance.UpdateValue(1000);

        }

    }
    private IEnumerator Move()
    {
        yield return new WaitForSeconds(1f);
        generated = false;
        animator.SetBool("Generated", generated);
        bc.enabled = true;
        Speed = 1.5f;
        rb.bodyType = RigidbodyType2D.Dynamic;
        StopCoroutine(Move());
    }
}
