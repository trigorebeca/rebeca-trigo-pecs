using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowerScript : MonoBehaviour
{

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag  == "jugador")
        {
            Destroy(gameObject);
            PlayerController.flower = true;
            TotalScoreManager.instance.UpdateValue(1000);

        }
    }
}
