using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFollower : MonoBehaviour
{
    public Transform Player;
    GameObject jugador;
    public Vector3 respawnPoint;

    public GameObject bound;

    public static bool teleporting;
    

    private void Start()
    {
        teleporting = false;
    }


    private void Update()
    {

        Vector3 position = transform.position;
        if (Player.transform.position.x > position.x)
        {
            position.x = Player.transform.position.x;
            transform.position = position;
        }
        //cuando el jugador respawnea o se teleporta

        if (teleporting & Player.transform.position.x < position.x)
        {
            bound.GetComponent<BoxCollider2D>().isTrigger = true;

            position.x = Player.transform.position.x;

            transform.position = position;
            teleporting = false;
        }
         if (!teleporting)
         {
            bound.GetComponent<BoxCollider2D>().isTrigger = false;

         }

        


    }

}

