using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager instance;
    public TextMeshProUGUI text;
    private int coinScore ;


    public int currentScore;
    private int totalScore;
    private string finalScore;

    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        
        currentScore = PlayerPrefs.GetInt("finalCoins");
       if (currentScore > 0)
        {
            UpdateValue(currentScore);

        }
        
    }
    public void UpdateValue(int coinValue)
    {
        coinScore += coinValue;
        text.text = "X" +  coinScore.ToString();
        PlayerPrefs.SetInt("finalCoins", coinScore);


    }

}
