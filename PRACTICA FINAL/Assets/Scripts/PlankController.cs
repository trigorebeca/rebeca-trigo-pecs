using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlankController : MonoBehaviour
{
    public Transform target;
    public float speed;



    // Update is called once per frame
    void Update()
    {
        Vector3 position = transform.position;
        Vector3 positionTarget = transform.position;

        if (transform.position.y > target.position.y)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }
        if (target.position.y <= -11f)
        {

            position.y = 8f;
            transform.position = position;
            positionTarget.y = 6f;
            target.position = positionTarget;
            transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }


    }
}
