using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : MonoBehaviour
{
    public int coinValue = 1;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("jugador"))
        {
            Destroy(gameObject);

            ScoreManager.instance.UpdateValue(coinValue);
            TotalScoreManager.instance.UpdateValue(100);

        }
    }
    
}
