using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PiranhaPlantController : MonoBehaviour
{
    public Transform target;
    public float speed;
    private Vector3 start, end;
    public LayerMask playerLayer;
    private bool isOut;
    GameObject jugador;
    private float distance;
    public Transform Player;


    // Start is called before the first frame update
    void Start()
    {
        if (target != null)
        {
            target.parent = null;
            start = transform.position;
            end = target.position;
        }
        jugador = GameObject.FindGameObjectWithTag("jugador");

    }

    // Update is called once per frame
    void Update()
    {
        if (Player.transform.position.x > transform.position.x)
        {
            distance = Player.transform.position.x - transform.position.x;


        } else if (Player.transform.position.x < transform.position.x)
        {
            distance = transform.position.x - Player.transform.position.x;

        }



        if (target != null)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }
        if (transform.position == target.position)
        {
            if (Physics2D.OverlapBox(transform.position, new Vector2(3f, 6.5f), 0, playerLayer) == null || !isOut)
            {
                StartCoroutine("DirectionChange");
            }

        }
        if (distance < 0.5f)
        {
            target.position = start;

        }

    }
    private IEnumerator DirectionChange()
    {
        isOut = !isOut;
        yield return new WaitForSeconds(2f);
        target.position = (target.position == start) ? end : start;
        StopCoroutine("DirectionChange");

    }




    private void OnCollisionEnter2D(Collision2D collision)

    {
        if (collision.gameObject.tag == "fireball")
        {
            Invoke("Death", 0);
        }


        if (collision.gameObject.CompareTag("jugador"))
        {
           if (!PlayerController.inmune && !PlayerController.star && !PlayerController.dead)
            {

                if (PlayerController.big)
                {
                    if (PlayerController.flower)
                    {
                        PlayerController.flower = false;
                        PlayerController.inmune = true;
                    }
                    PlayerController.big = false;
                    PlayerController.inmune = true;
                }
                else if (!PlayerController.star)

                {
                    PlayerController.inmune = true;

                    PlayerController.dead = true;
                }
            }
        }
    }

    public void Death()
    {
        TotalScoreManager.instance.UpdateValue(200);

        Destroy(gameObject);

    }
}
