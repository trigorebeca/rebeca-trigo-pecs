using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class TitleScreen : MonoBehaviour
{
    void Start()
    {
        PlayerPrefs.SetInt("finalScore", 0);
        PlayerPrefs.SetInt("finalCoins", 0);



    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            Change();
        }

    
    }

    void Change()
    {
        SceneManager.LoadScene("Main Menu");
    }
}
