using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlankUPController : MonoBehaviour
{

    public Transform target;
    public float speed;


    // Update is called once per frame
    void Update()
    {
        Vector3 position = transform.position;
        Vector3 positionTarget = transform.position;

        if (transform.position.y < target.position.y)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }
        if (target.position.y >= 8f)
        {

            position.y = -10.5f;
            transform.position = position;
            positionTarget.y = -8.5f;
            target.position = positionTarget;
            transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }


    }

}
