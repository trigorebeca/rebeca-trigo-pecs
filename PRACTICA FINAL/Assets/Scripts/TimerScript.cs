using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class TimerScript : MonoBehaviour
{
    public static TimerScript instance;
    public TextMeshProUGUI countdownDisplay;

    public int TimeLeft = 400;

    private string sceneName;


    void Start()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        sceneName = currentScene.name;

        if (sceneName == "Level1-1" || sceneName == "Level1-2" || sceneName == "Level1-2")
        {
            StartCoroutine(UpdateTimer());
        }

    }



    IEnumerator UpdateTimer()
    {
        while (TimeLeft> 0)
        {
            countdownDisplay.text = "TIME " + "\n" + TimeLeft.ToString();

            yield return new WaitForSeconds(1f);
            TimeLeft--;
        }
    }



}