using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoombaController : MonoBehaviour
{

    public float goombaSpeed;
    public bool moveRight;
    public bool isNot; //it is or it is not, when dead you are not
    public Animator animator;
    GameObject jugador;
    public GameObject goombaCollider;
    private Rigidbody2D rb;


    private void Start()
    {
        jugador = GameObject.FindGameObjectWithTag("jugador");
        rb = gameObject.GetComponent<Rigidbody2D>();

    }


    // Update is called once per frame
    void Update()
    {

        if (moveRight)
        {
            transform.Translate(2 * Time.deltaTime * goombaSpeed, 0, 0);

        }
        else
        {
            transform.Translate(-2 * Time.deltaTime * goombaSpeed, 0, 0);
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("tube") || collision.gameObject.CompareTag("enemigos"))
        {
            if (moveRight)
            {
                moveRight = false;
            }
            else if (!moveRight)
            {
                moveRight = true;
            }
        }

        if (collision.gameObject.tag == "jugador" && !PlayerController.dead)
        {

            Debug.Log("Debug here");

            //to calculate where to hit the goomba, since it only should kill it when hitting the top of the head
            float yOffset = 0.5f;
            if ((transform.position.y + yOffset) < collision.transform.position.y)
            {
                jugador.GetComponent<Rigidbody2D>().velocity = Vector2.up * 5;
                isNot = true;
                animator.SetBool("isNot", isNot);
                goombaSpeed = 0;
                goombaCollider.GetComponent<CircleCollider2D>().isTrigger = false;
                Invoke("Death", 1);
            }
            else if (!isNot && !PlayerController.star && !PlayerController.inmune)
            {
                if (PlayerController.big)
                {
                    if (PlayerController.flower)
                    {
                        PlayerController.flower = false;
                        PlayerController.inmune = true;


                    }
                    PlayerController.big = false;
                    PlayerController.inmune = true;
                }
                else 

                {
                    PlayerController.dead = true;
                    PlayerController.inmune = true;

                }
            }

        }
        if (collision.gameObject.tag == "fireball" || collision.gameObject.CompareTag("Koopa"))
        {
            Vector2 goombaScale = gameObject.transform.localScale;
            goombaScale.y = -1;
            transform.localScale = goombaScale;
            goombaSpeed = 0;

            Invoke("Death", 5f);

            // in the tutorial he creates a static animation but 1 line of code is faster
            gameObject.GetComponent<Animator>().enabled = false;
            GetComponent<Collider2D>().isTrigger = true;


        }

    }
    public void Death()
    {
        TotalScoreManager.instance.UpdateValue(100);

        Destroy(gameObject);

    }
}


