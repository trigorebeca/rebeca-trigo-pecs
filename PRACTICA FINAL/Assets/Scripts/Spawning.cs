using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawning : MonoBehaviour
{

    public bool goombaSpawn;
    public bool underGoombaSpawn;
    public bool koopaSpawn;
    public bool underKoopaSpawn;
    public GameObject goomba;
    public GameObject underGoomba;
    public GameObject koopa;
    public GameObject underKoopa;
    public Transform objectSpawn;
    public Transform Player;

    private float offset = 8f;
    private float distance;




    private void Start()
    {
    distance = objectSpawn.position.x - offset;
    }

private void Update()
    {
        if (Player.transform.position.x >= distance)
        {
            if (goomba != null && goombaSpawn == true)
            {
                Instantiate(goomba, objectSpawn.position, objectSpawn.rotation);
                //goombaSpawn = false;
                Destroy(gameObject);

            }
            if (underGoomba != null && underGoombaSpawn == true)
            {
                Instantiate(underGoomba, objectSpawn.position, objectSpawn.rotation);
               // underGoombaSpawn = false;
                Destroy(gameObject);

            }
            if (koopa != null && koopaSpawn == true)
            {
                Instantiate(koopa, objectSpawn.position, objectSpawn.rotation);
               // koopaSpawn = false;
                Destroy(gameObject);

            }
            if (underKoopa != null && underKoopaSpawn == true)
            {
                Instantiate(underKoopa, objectSpawn.position, objectSpawn.rotation);
                underKoopaSpawn = false;
                Destroy(gameObject);
            }
        }

    }


}
