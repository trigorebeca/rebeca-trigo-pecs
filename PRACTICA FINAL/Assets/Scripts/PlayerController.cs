using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    //public CircleCollider2D cuerpo;
    //  public BoxCollider2D mano;
    // public GameObject fire; I decided not to change the height where he fires from 

    public GameObject colliderSmall;
    public GameObject colliderBig;


    private float originalSpeed;
    private float originalJump;

    private Rigidbody2D rb2D;
    public float playerSpeed;
    private bool facingRight = true;
    private bool canDoubleJump;
    private bool isGrounded;
    private float moveHorizontal;
    public float jumpHeight;
    public Animator animator;
    public Transform pies;
    public float checkRadius;
    public LayerMask whichGround;
    private float jumpCounter;//counter for how long the jump key is pressed
    public float jumpTime;
    private bool isJumping;
    public static bool dead;
    //private float countdown = 0.27f;
    GameObject jugador;
    public static int lifeCounter;
    public static bool big;
    public static bool star;
    private static bool bent;
    public static bool flower;
    private static bool fell;
    public static bool inmune;



    private GameObject currentTeleporter;

    private float starTimer = 12f;
    static float inmuneTimer = 2f;


    public Transform fireSpawn;
    public GameObject fireBall;
    public GameObject playerPrefab;

    public  Vector3 respawnPoint;

    private GameObject currentSpace;

    public static bool respawn;
    public string sceneName;




    private void Start()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        sceneName = currentScene.name;

        rb2D = gameObject.GetComponent<Rigidbody2D>();
        //statuses of mario caused by boosters
        dead = false;
        big = false;
        bent = false;
        flower = false;
        respawnPoint = transform.position;
        respawn = false;
        fell = false;
        inmune = false;
        lifeCounter = 3;//these are the extra lives 2 extra lives that you revive from plus a regular one that kills you
        originalJump = jumpHeight;
        originalSpeed = playerSpeed;
    }

    void FixedUpdate()
    {
        moveHorizontal = Input.GetAxisRaw("Horizontal");
        animator.SetFloat("Speed", Mathf.Abs(moveHorizontal));
        rb2D.velocity = new Vector2(moveHorizontal * playerSpeed, rb2D.velocity.y);
    }

    void Update()
    {
        isGrounded = Physics2D.OverlapCircle(pies.position, checkRadius, whichGround);
        animator.SetBool("isGrounded", isGrounded);


        if (moveHorizontal < 0.0f && facingRight)
        {
            FlipPlayer();
        }

        if (moveHorizontal > 0.0 && !facingRight)
        {
            FlipPlayer();
        }

        if (currentSpace != null)
        {
            respawnPoint = currentSpace.GetComponent<Respawner>().GetDestination().position;

        }




        if (Input.GetKeyDown(KeyCode.J))
        {
            if (currentTeleporter != null)
            {
                transform.position = currentTeleporter.GetComponent<TubePortation>().GetDestination().position;
            }
        }

        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            isJumping = true;
            Jump();
        }
        else
        {
            if (Input.GetKey(KeyCode.Space) && isJumping)
            {
                if (jumpCounter > 0)
                {
                    rb2D.velocity = (Vector2.up * jumpHeight);
                    jumpCounter -= Time.deltaTime;
                }
                else
                {
                    isJumping = false;
                }
            }
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            isJumping = false;
        }


        if (dead)
        {
            Debug.Log(lifeCounter);

            playerSpeed = 0;
            jumpHeight = 0;
            animator.SetTrigger("Death");
            //countdown -= Time.deltaTime;
           // if (countdown > 0f)
            //{
                Invoke("Death", 0.5f);
            //
            
            if (lifeCounter == 1)
            {
                Invoke("GameOver", 3);
            }
            else
            {
                respawn = true;
                Invoke("Respawn", 1.5f);
            }
            dead = false;

        }




        if (big)
        {
            colliderBig.SetActive(true);

            colliderSmall.SetActive(false);
         

            animator.SetBool("Big", big);
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                bent = true;
                animator.SetBool("Bent", bent);
                playerSpeed = 0;
            }
            else if (Input.GetKeyUp(KeyCode.DownArrow))
            {
                bent = false;
                animator.SetBool("Bent", bent);
                playerSpeed = originalSpeed;
            }

        }
        else
        {

                colliderSmall.SetActive(true);
                colliderBig.SetActive(false);
            

            animator.SetBool("Big", big);
        }

        if (flower && !big)
        {
            big = true;
            flower = false;
        }
        else if (flower && big)
        {


            animator.SetBool("Flower", flower);

            if (Input.GetKeyDown(KeyCode.C))
            {
                animator.SetBool("Shooting", true);
                Instantiate(fireBall, fireSpawn.position, fireSpawn.rotation);

            }
            else if (Input.GetKeyUp(KeyCode.C))
            {
                animator.SetBool("Shooting", false);
            }
        }
        else if (!flower && !big)
        {
            animator.SetBool("Big", big);
            animator.SetBool("Flower", flower);


        }

        if (inmune)
        {
            inmuneTimer -= Time.deltaTime * 1;
            if (inmuneTimer < 0)
            {
                inmune = false;
                inmuneTimer = 2f;
            }
        }



        if (star)
        {
            starTimer -= Time.deltaTime * 1;
            if (starTimer > 0)
            {
                animator.SetBool("Star", star);

            }
            else
            {
                star = false;
                animator.SetBool("Star", star);
                starTimer = 12f;

            }
        }

    }

    void FlipPlayer()
    {
        facingRight = !facingRight;
        Vector2 playerScale = gameObject.transform.localScale;
        playerScale.x *= -1;
        transform.localScale = playerScale;
    }

    void Jump()
    {
        animator.SetBool("isGrounded", isGrounded);
        rb2D.velocity = (Vector2.up * jumpHeight);
        isJumping = true;
        jumpCounter = jumpTime;

    }



    void GameOver()
    {
        PlayerPrefs.SetInt("finalScore", 0);
        PlayerPrefs.SetInt("finalCoins", 0);
        SceneManager.LoadScene("GameOver");
    }
    void Won()
    {
        if (sceneName == "Level1-1")
        {
            SceneManager.LoadScene("Level1-2");

        }
        else if(sceneName == "Level1-2")
        {
            SceneManager.LoadScene("Won");
            PlayerPrefs.SetInt("finalScore", 0);
            PlayerPrefs.SetInt("finalCoins", 0);
        }

    }

    public void Death()
    {
        colliderSmall.SetActive(false);
        colliderBig.SetActive(false);
        rb2D.velocity = new Vector2(rb2D.velocity.x, 5f);
        lifeCounter--;

    }


    public void Respawn()
    {


        playerSpeed = originalSpeed;
        jumpHeight = originalJump;
        inmune = false;
        colliderSmall.SetActive(true);
        colliderBig.SetActive(false);

        if (currentSpace != null)
        {

            transform.position = (respawnPoint);

        }
        else
        {
            transform.position = (respawnPoint);
        }
        rb2D.velocity = new Vector2(rb2D.velocity.x, 0.2f);
        animator.SetBool("Death", dead);
        fell = false;

        PlayerFollower.teleporting = true;

        respawn = false;


    }



    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.CompareTag("ladrillo") || collision.gameObject.CompareTag("question"))
        {
            isJumping = false; 
            rb2D.velocity = Vector2.down * 3;
        }
        if (collision.gameObject.CompareTag("entrance"))
        {
            currentTeleporter = collision.gameObject;
            PlayerFollower.teleporting = true;
        }
        else if (collision.gameObject.tag == "exit")
        {
            currentTeleporter = collision.gameObject;
            PlayerFollower.teleporting = true;

        }
        else
        {
            currentTeleporter = null;
        }

        if (collision.gameObject.CompareTag("checkpoint"))
        {
            currentSpace = collision.gameObject;
        }
        else
        {
            currentSpace = null;
        }
        if (collision.gameObject.CompareTag("void") && !dead && !fell)
        {
            dead = true;
            fell = true;
        }
        if (collision.gameObject.CompareTag("goal"))
        {

            Invoke("Won", 0.1f);

        }


    }


}