using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour

{


    public void GoToMain()
    {
        SceneManager.LoadScene("Main Menu");

    }
    public void ReadCreds()
    {
        SceneManager.LoadScene("Credits");

    }
    public void Exit()
    {

        Application.Quit();

    }
    public void StartNewGame()
    {

        SceneManager.LoadScene("Level1-1");

    }

}

