using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MovementController : MonoBehaviour
{
    private Rigidbody2D rb2D;
    public float playerSpeed = 2.0f;
    private bool facingRight = true;
    private bool canDoubleJump;
    private bool isGrounded;
    private float moveHorizontal;
    public int jumpHeight = 200  ;
    public Animator animator;
    public Transform pies;
    public float checkRadius;
    public LayerMask whichGround;
    private float jumpCounter;//counter for how long the jump key is pressed
    public float jumpTime;
    private bool isJumping;
    public static bool dead;
    private float countdown = 0.27f;
    GameObject jugador;
    public int lifeCounter = 3;

    private void Start()
    {
        rb2D = gameObject.GetComponent<Rigidbody2D>();
        dead = false;
        

    }
    void FixedUpdate()
    {
        moveHorizontal = Input.GetAxisRaw("Horizontal");
        animator.SetFloat("Speed", Mathf.Abs(moveHorizontal));
        rb2D.velocity = new Vector2(moveHorizontal * playerSpeed, rb2D.velocity.y);
    }

    void Update()
    {
        isGrounded = Physics2D.OverlapCircle(pies.position, checkRadius, whichGround);
        animator.SetBool("isGrounded", isGrounded);

      
        if (moveHorizontal < 0.0f && facingRight)
        {
            FlipPlayer();
        }

        if (moveHorizontal > 0.0 && !facingRight)
        {
            FlipPlayer();
        }


        
     if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
     {
         isJumping = true;
         Jump();
     }
     else
     {
         if (Input.GetKey(KeyCode.Space) && isJumping)
         {
             if (jumpCounter > 0)
             {
                 rb2D.velocity = (Vector2.up * jumpHeight);
                 jumpCounter -= Time.deltaTime;
             }
             else
             {
                 isJumping = false;
             }
         }
     }
     
     if(Input.GetKeyUp(KeyCode.Space))
     {
         isJumping = false;
     }

    if (dead)
    {
            isJumping = false;
            animator.SetBool("Death", dead);
            countdown -= Time.deltaTime;
            if (countdown> 0f)
            {
                Invoke("Death", 0.5f);
            }

            if (transform.position.y <-30)
            {
                //rename scene in future so it looks cooler/nicer
                SceneManager.LoadScene("Level1-1");
            }
            playerSpeed = 0;
            jumpHeight = 0;
            lifeCounter--;


         }
        //so it does die when falling down the floor
        if (transform.position.y <= 0.6f)
        {
           dead = true;
        }
        if (lifeCounter == 0)
        {
            SceneManager.LoadScene("GameOver");
        }
        if (transform.position.x >= 67.7f)
        {
            SceneManager.LoadScene("Won");
        }

    }

    void FlipPlayer()
    {
        facingRight = !facingRight;
        Vector2 playerScale = gameObject.transform.localScale;
        playerScale.x *= -1;
        transform.localScale = playerScale;
    }
    
    void Jump()
    {
        animator.SetBool("isGrounded", isGrounded);
        rb2D.velocity = (Vector2.up * jumpHeight);
        isJumping = true;
        jumpCounter = jumpTime;
        
    }
    void Death()
    {
        GetComponent<Collider2D>().isTrigger = true;
        rb2D.velocity = new Vector2(rb2D.velocity.x, 5f);
    }
    

}