using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBallController : MonoBehaviour
{
    public float speed = 0.5f;
    private Rigidbody2D rb;
    private Transform jugador;

    public float jumpPower = 10f;
    public bool isGrounded;
    public Transform feetPos;
    public float checkRadius;
    public LayerMask WhatIsGround;

    public Animator animator;
    public bool death;


    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        jugador = GameObject.FindGameObjectWithTag("jugador").transform;

    }

    private void Start()
    {
        if (jugador.localScale.x < 0)
        {
            speed = -speed;
        }

    }


    // Update is called once per frame
    void Update()
    {

        rb.velocity = new Vector2(speed, rb.velocity.y);

        isGrounded = Physics2D.OverlapCircle(feetPos.position, checkRadius, WhatIsGround);

        if (isGrounded)
        {
            rb.velocity = Vector2.up * jumpPower;
        }



    }

    

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "tube" || collision.gameObject.tag == "enemigos")
        {
            death = true;
            animator.SetBool("Death", death);
            Invoke("Death", 0.4f);

        }
        if (collision.gameObject.tag == "void")
        {
            Invoke("Death", 0.4f);
            speed = 0;
            GetComponent<Collider2D>().isTrigger = true;

        }
    }

    private void Death()
    {
        Destroy(gameObject);

    }

}
