using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public BoxCollider2D cuerpo;
    public BoxCollider2D mano;
   // public GameObject fire; I decided not to change the height where he fires from 


    private Rigidbody2D rb2D;
    public float playerSpeed = 2.0f;
    private bool facingRight = true;
    private bool canDoubleJump;
    private bool isGrounded;
    private float moveHorizontal;
    public int jumpHeight;
    public Animator animator;
    public Transform pies;
    public float checkRadius;
    public LayerMask whichGround;
    private float jumpCounter;//counter for how long the jump key is pressed
    public float jumpTime;
    private bool isJumping;
    public static bool dead;
    //private float countdown = 0.27f;
    GameObject jugador;
    public static int lifeCounter;
    public static bool big;
    public static bool star;
    private static bool bent;
    public static bool flower;
    private static bool fell;
    public static bool inmune;



    private GameObject currentTeleporter;

    private float starTimer = 12f;
    static float inmuneTimer = 2f;


    public Transform fireSpawn;
    public GameObject fireBall;
    public GameObject playerPrefab;

    public  Vector3 respawnPoint;

    private GameObject currentSpace;

    public static bool respawn;

   

    private void Start()
    {
        rb2D = gameObject.GetComponent<Rigidbody2D>();
        cuerpo = gameObject.GetComponent<BoxCollider2D>();
        //statuses of mario caused by boosters
        dead = false;
        big = false;
        bent = false;
        flower = false;
        respawnPoint = transform.position;
        respawn = false;
        fell = false;
        inmune = false;
        lifeCounter = 3;//these are the extra lives 2 extra lives that you revive from plus a regular one that kills you
    }

    void FixedUpdate()
    {
        moveHorizontal = Input.GetAxisRaw("Horizontal");
        animator.SetFloat("Speed", Mathf.Abs(moveHorizontal));
        rb2D.velocity = new Vector2(moveHorizontal * playerSpeed, rb2D.velocity.y);
    }

    void Update()
    {
        isGrounded = Physics2D.OverlapCircle(pies.position, checkRadius, whichGround);
        animator.SetBool("isGrounded", isGrounded);


        if (moveHorizontal < 0.0f && facingRight)
        {
            FlipPlayer();
        }

        if (moveHorizontal > 0.0 && !facingRight)
        {
            FlipPlayer();
        }

        if (currentSpace != null)
        {
            respawnPoint = currentSpace.GetComponent<Respawner>().GetDestination().position;

        }




        if (Input.GetKeyDown(KeyCode.J))
        {
            if (currentTeleporter != null)
            {
                transform.position = currentTeleporter.GetComponent<TubePortation>().GetDestination().position;
            }
        }

        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            isJumping = true;
            Jump();
        }
        else
        {
            if (Input.GetKey(KeyCode.Space) && isJumping)
            {
                if (jumpCounter > 0)
                {
                    rb2D.velocity = (Vector2.up * jumpHeight);
                    jumpCounter -= Time.deltaTime;
                }
                else
                {
                    isJumping = false;
                }
            }
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            isJumping = false;
        }


        if (dead)
        {
            Debug.Log(lifeCounter);

            playerSpeed = 0;
            jumpHeight = 0;
            animator.SetTrigger("Death");
            //countdown -= Time.deltaTime;
           // if (countdown > 0f)
            //{
                Invoke("Death", 0.5f);
            //
            
            if (lifeCounter == 1)
            {
                Invoke("GameOver", 3);
            }
            else
            {
                respawn = true;
                Invoke("Respawn", 2);
            }
            dead = false;
        }

        if (transform.position.x >= 67.7f && transform.position.x <= 69f)
        {
            Invoke("Won", 0);
        }


        if (big)
        {
            if (cuerpo.size.x != 0.3f)
            {
                Grow();
            }

            animator.SetBool("Big", big);
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                bent = true;
                animator.SetBool("Bent", bent);
                playerSpeed = 0;
            }
            else if (Input.GetKeyUp(KeyCode.DownArrow))
            {
                bent = false;
                animator.SetBool("Bent", bent);
                playerSpeed = 2.0f;
            }

        }
        else
        {
            if (cuerpo.size.x != 0.0004414618f)
            {
                Shrink();
            }

            animator.SetBool("Big", big);
        }

        if (flower && !big)
        {
            big = true;
            flower = false;
        }
        else if (flower && big)
        {


            animator.SetBool("Flower", flower);

            if (Input.GetKeyDown(KeyCode.C))
            {
                animator.SetBool("Shooting", true);
                Instantiate(fireBall, fireSpawn.position, fireSpawn.rotation);

            }
            else if (Input.GetKeyUp(KeyCode.C))
            {
                animator.SetBool("Shooting", false);
            }
        }
        else if (!flower && !big)
        {
            animator.SetBool("Big", big);
            animator.SetBool("Flower", flower);


        }

        if (inmune)
        {
            inmuneTimer -= Time.deltaTime * 1;
            if (inmuneTimer < 0)
            {
                inmune = false;
                inmuneTimer = 2f;
            }
        }



        if (star)
        {
            starTimer -= Time.deltaTime * 1;
            if (starTimer > 0)
            {
                animator.SetBool("Star", star);
       
            }
            else
            {
                star = false;
                animator.SetBool("Star", star);
                starTimer = 12f;
            }
        }

    }

    void FlipPlayer()
    {
        facingRight = !facingRight;
        Vector2 playerScale = gameObject.transform.localScale;
        playerScale.x *= -1;
        transform.localScale = playerScale;
    }

    void Jump()
    {
        animator.SetBool("isGrounded", isGrounded);
        rb2D.velocity = (Vector2.up * jumpHeight);
        isJumping = true;
        jumpCounter = jumpTime;

    }
    void Grow()
    {
            cuerpo.size = new Vector2(0.3f, 0.65f);
            cuerpo.offset = new Vector2(0f, 0.17f);

            mano.size = new Vector2(0.3f, 0.1f);
            mano.offset = new Vector2(0f, 0.45f);
    }
    void Shrink()
    {
        cuerpo.size = new Vector2(0.2626667f, 0.3215331f);
        cuerpo.offset = new Vector2(0.0004414618f, 0.002558634f);

        mano.size = new Vector3(0.2116407f, 0.02664089f);
        mano.offset = new Vector3(-0.01256522f, 0.1516282f);
    }


    void GameOver()
    {
        SceneManager.LoadScene("GameOver");
    }
    void Won()
    {
        SceneManager.LoadScene("Won");
    }

    public void Death()
    {
        GetComponent<Collider2D>().isTrigger = true;
        rb2D.velocity = new Vector2(rb2D.velocity.x, 5f);
        lifeCounter--;

    }


    public void Respawn()
    {


        playerSpeed = 2;
        jumpHeight = 4;
        inmune = false;
        GetComponent<Collider2D>().isTrigger = false;
        if (currentSpace != null)
        {

            transform.position = (respawnPoint);

        }
        else
        {
            transform.position = (respawnPoint);
        }
        rb2D.velocity = new Vector2(rb2D.velocity.x, 0.2f);
        animator.SetBool("Death", dead);
        fell = false;
        PlayerFollower.respawningCam = true;

        respawn = false;


    }



    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.CompareTag("ladrillo") || collision.gameObject.CompareTag("question"))
        {
            isJumping = false; 
            rb2D.velocity = Vector2.down * 3;
        }
        if (collision.gameObject.CompareTag("entrance"))
        {
            currentTeleporter = collision.gameObject;
        }
        else if (collision.gameObject.tag == "exit")
        {
            currentTeleporter = collision.gameObject;

        }
        else
        {
            currentTeleporter = null;
        }

        if (collision.gameObject.CompareTag("checkpoint"))
        {
            currentSpace = collision.gameObject;
        }
        else
        {
            currentSpace = null;
        }
        if (collision.gameObject.CompareTag("void") && !dead && !fell)
        {
            dead = true;
            fell = true;
        }



    }


}