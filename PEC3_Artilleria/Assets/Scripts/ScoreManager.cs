using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager instance;
    public TextMeshProUGUI text;
    private int coinScore = 0;
    // Start is called before the first frame update
    void Start()
    {
        if(instance == null)
        {
            instance = this;
        }
        
    }
    public void UpdateValue(int coinValue)
    {
        coinScore++;//= coinValue;
        text.text = "X" + coinScore.ToString();
    }
 
}
