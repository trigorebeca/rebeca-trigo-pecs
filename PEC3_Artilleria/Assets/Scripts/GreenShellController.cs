using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenShellController : MonoBehaviour
{
    private int speed;
    private bool moveRight;
    GameObject jugador;
    private bool shellAttack;

    private Rigidbody2D rb;

    public Transform koopaSpawn;


    public GameObject koopa;
    public Animator animator;
    private bool revive;

    // Start is called before the first frame update
    void Start()
    {
        jugador = GameObject.FindGameObjectWithTag("jugador");
        rb = gameObject.GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        revive = false;

    }

    // Update is called once per frame
    void Update()
    {
        if (moveRight)
        {
            transform.Translate(Time.deltaTime * speed, 0, 0);

        }
        else if (!moveRight)
        {
            transform.Translate(-1 * Time.deltaTime * speed, 0, 0);
        }
        if (speed == 0)
        {
            StartCoroutine("Revive");
        } if (speed != 0)
        {
            StartCoroutine("Revive");
            StopCoroutine("Revive");

        }

    }

    private IEnumerator Revive()
    {
        yield return new WaitForSeconds(5f);
        if (revive)
        {
            animator.SetBool("Revive", revive);
            yield return new WaitForSeconds(2f);
            revive = false;
            animator.SetBool("Revive", revive);
        }
        else
        {
        Instantiate(koopa, koopaSpawn.position, koopaSpawn.rotation);
        Destroy(gameObject);
        }

    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("jugador"))
        {
            if (shellAttack)
            {
                float yOffset = 0.25f;
                if (transform.position.y + yOffset < collision.transform.position.y)
                {
                    jugador.GetComponent<Rigidbody2D>().velocity = Vector2.up * 3;
                    speed = 0;
                    shellAttack = false;
                    gameObject.tag = "enemigos";
                }
                else if (!PlayerController.inmune && !PlayerController.star)
                {
                    if (PlayerController.big)
                    {
                        if (PlayerController.flower)
                        {
                            PlayerController.flower = false;
                            PlayerController.inmune = true;
                        }
                        PlayerController.big = false;
                        PlayerController.inmune = true;
                    }
                    else if (!PlayerController.star)

                    {
                        PlayerController.dead = true;
                        PlayerController.inmune = true;

                    }
                }
            }
            else
            {
                float xOffset = 0.25f;
                if ((transform.position.x + xOffset) < collision.transform.position.y)
                {
                    speed = 3;
                    moveRight = false;
                    shellAttack = true;
                    gameObject.tag = "Koopa";
                }
                else
                {
                    speed = 3;
                    moveRight = true;
                    shellAttack = true;
                    gameObject.tag = "Koopa";

                }
            }
        }

    
        if (collision.gameObject.gameObject.tag == "tube")
        {
           if (moveRight)
           {
                moveRight = false;
           }
           else if (!moveRight)
           {
               moveRight = true;
           }
        }
        if (collision.gameObject.tag == "fireball" || collision.gameObject.CompareTag("Koopa"))
        {

            GetComponent<Collider2D>().isTrigger = true;

            // transform.localScale = goombaScale;
            speed = 0;
            Invoke("Death", 5f);


        }



    }

    public void Death()
    {
        Destroy(gameObject);

    }

}
