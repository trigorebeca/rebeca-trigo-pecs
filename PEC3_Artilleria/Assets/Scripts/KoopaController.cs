using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KoopaController : MonoBehaviour
{
    public float speed;
    public bool moveRight = false;

    public Animator animator;

    public Transform shellSpawn;
    public GameObject shell;
    private GameObject jugador;

    private Rigidbody2D rb;
    public GameObject koopaCollider;



    // Start is called before the first frame update
    void Start()
    {
        jugador = GameObject.FindGameObjectWithTag("jugador");
       // rb = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

            if (moveRight)
            {
                transform.Translate(2 * Time.deltaTime * speed, 0, 0);

            }
            else if (!moveRight)
            {
                transform.Translate(-2 * Time.deltaTime * speed, 0, 0);
            }
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("tube") || collision.gameObject.CompareTag("enemigos"))
        {
            if (moveRight)
            {
                Flip();
            }
            else if (!moveRight)
            {
                Flip();
            }
        }

        if (collision.gameObject.CompareTag("jugador"))
        {
            float yOffset = 0.25f;
            if ((transform.position.y + yOffset) < collision.transform.position.y)
            {
                jugador.GetComponent<Rigidbody2D>().velocity = Vector2.up * 5;
                Instantiate(shell, shellSpawn.position, shellSpawn.rotation);
                Destroy(gameObject);
            }
            else if (!PlayerController.inmune && !PlayerController.star && !PlayerController.dead)
            { 

                if (PlayerController.big)
                {
                    if (PlayerController.flower)
                    {
                        PlayerController.flower = false;
                        PlayerController.inmune = true;
                    }
                    PlayerController.big = false;
                    PlayerController.inmune = true;
                }
                else if (!PlayerController.star)

                {
                    koopaCollider.GetComponent<BoxCollider2D>().isTrigger = false;
                    PlayerController.inmune = true;

                    PlayerController.dead = true;
                }
            }
        }


        
        if (collision.gameObject.tag == "fireball" || collision.gameObject.CompareTag("Koopa"))
        {
            animator.SetBool("Death", true);
            GetComponent<Collider2D>().isTrigger = true;

            // transform.localScale = goombaScale;
            speed = 0;
            Invoke("Death", 1f);

            // in the tutorial he creates a static animation but 1 line of code is faster
            gameObject.GetComponent<Animator>().enabled = false;

        }

    }
    void Flip()
    {
        moveRight = !moveRight;
        Vector2 ls = gameObject.transform.localScale;
        ls.x *= -1;
        transform.localScale = ls;
    }
    public void Death()
    {
        Destroy(gameObject);

    }

}
