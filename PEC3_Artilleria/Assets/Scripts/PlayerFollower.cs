using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFollower : MonoBehaviour
{
    public Transform Player;
    GameObject jugador;
    public Vector3 respawnPoint;

    public GameObject bound;
    public static bool respawningCam;


    private void Start()
    {
        respawningCam = false;
    }


    private void Update()
    {

        Vector3 position = transform.position;
        if (Player.transform.position.x > position.x && Player.transform.position.x <= 66F)
        {
            position.x = Player.transform.position.x;
            transform.position = position;
        }
        //cuando va al nivel subterraneo
        if (Player.transform.position.x >= 69F )
        {
            position.x = 73.8f;
            transform.position = position;
        }
        //cuando regresa
        if (Player.transform.position.x < position.x && position.x == 73.8f)
        {
            position.x = Player.transform.position.x;
            transform.position = position;
        }
        //cuando el jugador respawnea


        if (respawningCam)
        {
            bound.GetComponent<BoxCollider2D>().isTrigger = true;
            position.x = Player.transform.position.x;
            transform.position = position;
            respawningCam = false;
        }
        if (!respawningCam)
        {
            bound.GetComponent<BoxCollider2D>().isTrigger = false;

        }







    }

}
